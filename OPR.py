# ReplyKeyboardMarkup'

import time

import requests

import sqlite3

import telepot
import telepot.helper
from telepot.loop import MessageLoop
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton
from telepot.namedtuple import ReplyKeyboardMarkup, KeyboardButton
from telepot.delegate import (
    per_chat_id, create_open, pave_event_space, include_callback_query_chat_id)




class Sheduler(telepot.helper.ChatHandler):
    markup = ReplyKeyboardMarkup(keyboard=[
                     [KeyboardButton(text='New group'),KeyboardButton(text='Select group')],

                      [KeyboardButton(text='My groups'), KeyboardButton(text='Шо там завтра')],

                 ], resize_keyboard = True)

    cancel_markup = ReplyKeyboardMarkup(keyboard = [
        [KeyboardButton(text = 'Cancel')],
    ], resize_keyboard = True)

    def __init__(self, *args, **kwargs):
        super(Sheduler, self).__init__(*args, **kwargs)

        # Retrieve from database

        self._cur_group = None

        self._state = 0 # current state of programm


    def _cancel_last(self):
        if self._editor:
            self._editor.editMessageReplyMarkup(reply_markup=None)
            self._editor = None
            self._edit_msg_ident = None

    def on_chat_message(self, msg):

        if msg['text'] == '/start':
            sent = self.sender.sendMessage('Welcome to', reply_markup=self.markup)
        elif msg['text'] == 'New group':
            self._state = 1
            self.sender.sendMessage('New Group', reply_markup = self.cancel_markup)
            self.sender.sendMessage('Enter group name:', reply_markup= None)
        else:

            if self._state == 1:

                con = sqlite3.connect('data.db')

                try:


                    curr = con.cursor()

                    curr.execute('INSERT INTO groups (name) VALUES (?)',(msg['text']))

                    con.commit()

                    self._state = 0
                except sqlite3.Error as e:

                    if con:
                        con.rollback()

                    print("Error " + e.args[0])

                    self.sender.sendMessage('Group with this name already exist', reply_markup=self.cancel_markup)
                    self.sender.sendMessage('Enter group name:', reply_markup=None)
                finally:
                    if con:
                        con.close()


    def on_callback_query(self, msg):
        query_id, from_id, query_data = telepot.glance(msg, flavor='callback_query')

        if query_data == 'yes':
            self._cancel_last()
            self.sender.sendMessage('Thank you!')
            self.close()
        else:
            self.bot.answerCallbackQuery(query_id, text='Ok. But I am going to keep asking.')
            self._cancel_last()
            self._propose()

    def on__idle(self, event):

        self.close()

    def on_close(self, ex):
        return

TOKEN = '342080385:AAHQRtFEyegac8pFw7WTBQBltmNc79JJvB0'


bot = telepot.DelegatorBot(TOKEN, [
    include_callback_query_chat_id(
        pave_event_space())(
            per_chat_id(types=['private']), create_open, Sheduler, timeout=30),
])
MessageLoop(bot).run_as_thread()
print('Listening ...')

while 1:
    time.sleep(10)



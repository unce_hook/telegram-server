# ReplyKeyboardMarkup'

import time

import requests

import json

from bs4 import BeautifulSoup

import telepot
import telepot.helper
from telepot.loop import MessageLoop
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton
from telepot.namedtuple import ReplyKeyboardMarkup, KeyboardButton, ReplyKeyboardRemove
from telepot.delegate import (
    per_chat_id, create_open, pave_event_space, include_callback_query_chat_id)

times = [
    ['8:15', '9:05'],
    ['10:00', '10:50'],
    ['11:45', '12:35', '13:25'],
    ['14:20', '15:10'],
    ['16:05', '16:55'],
    ['17:50', '18:40'],
]

days = [
    'Понедельник',
    'Вторник',
    'Среда',
    'Четверг',
    'Пятница',
    'Суббота',
    'Воскресенье'

]


def group_search(my_group):
    '''
    Поиск ссылки расписания для заданной группы
    :param my_group: название группы, например ит-41
    :return: ссылка на расписание группы или False если группы нет
    '''
    shedule_head = 'http://www.bstu.ru/about/useful/schedule'

    r = requests.get(shedule_head)

    doc = r.text

    soup = BeautifulSoup(doc, 'lxml')

    groups = soup.find_all('div', 'schedule-group-item')

    for group in groups:

        gr = BeautifulSoup(str(group), 'lxml')

        links = gr.find_all('a')

        for li in links:
            if li.text.lower() == my_group.lower():
                return str(li['href'])

    return False


def current_week():
    '''
    Определяет является текущая неделя числителем или знаменателем
    :return:  1 - числитель 2 - знаменатель
    '''
    t = time.gmtime(time.time())

    if t[6] == 6:
        week = (t[7] - 1) // 7
    else:
        week = (t[7] - 1) // 7 + 1

    week = 1 if week % 2 == 1 else 2

    return week


def first_para(link, day):
    '''
    Определяет 1 пару в заданный день недели
    :param link: ссылка на страницу с расписанием группы
    :param day: номер дня недели от 0(понедельник) до 6 (воскресенье), при не соблюдении условия функция вернет False
    :return: сформировананя строка, где указанна название пары, время, кабинет если есть и препод
    '''
    if day > 6 or day < 0:
        return False

    r = requests.get(link)

    r.encoding = 'utf-8'

    doc = r.text

    soup = BeautifulSoup(doc, 'lxml')

    week = current_week()

    if day == 6:
        return 'Ввоскресенье же, отдыхай'

    table = soup.find_all('td', 'schedule_std')

    n = len(table) // 6

    curr = []

    for i in range(n):
        curr.append(table[day + i * n])

    for i in range(6):
        if curr[i].text != ' ':

            cell = BeautifulSoup(str(curr[i]), 'lxml')

            quarts = cell.find_all('td', 'schedule_quarter')

            halfs = cell.find_all('td', 'schedule_half')

            hqs = cell.find_all('td', 'schedule_hq')

            if quarts != [] and len(quarts) == 4:
                if quarts[0].text != ' ' and week == 1:

                    qb = BeautifulSoup(str(quarts[0]), 'lxml')

                    place = qb.find('div', 'place_quarter').text

                    subject = qb.find('div', 'subject_quarter').text

                    sp = qb.find('div', 'sp_quarter').text

                    return days[day] + '\nПолупара ' + subject + '\nв ' + times[i][
                        0] + '\n' + place + '\nпреподаватель ' + sp

                elif quarts[1].text != ' ' and week == 2:

                    qb = BeautifulSoup(str(quarts[0]), 'lxml')

                    place = qb.find('div', 'place_quarter').text

                    subject = qb.find('div', 'subject_quarter').text

                    sp = qb.find('div', 'sp_quarter').text

                    return days[day] + '\nПолупара ' + subject + '\nв ' + times[i][
                        0] + '\n' + place + '\nпреподаватель ' + sp

                if quarts[2].text != ' ' and week == 1:

                    qb = BeautifulSoup(str(quarts[0]), 'lxml')

                    place = qb.find('div', 'place_quarter').text

                    subject = qb.find('div', 'subject_quarter').text

                    sp = qb.find('div', 'sp_quarter').text

                    return days[day] + '\nПолупара ' + subject + '\nв ' + times[i][
                        0] + '\n' + place + '\nпреподаватель ' + sp

                elif quarts[3].text != ' ' and week == 2:

                    qb = BeautifulSoup(str(quarts[0]), 'lxml')

                    place = qb.find('div', 'place_quarter').text

                    subject = qb.find('div', 'subject_quarter').text

                    sp = qb.find('div', 'sp_quarter').text

                    return days[day] + '\nПолупара ' + subject + '\nв ' + times[i][
                        0] + '\n' + place + '\nпреподаватель ' + sp

            elif halfs != [] and len(halfs) == 2:

                if halfs[0].text != ' ' and week == 1:

                    hb = BeautifulSoup(str(halfs[0]), 'lxml')

                    place = hb.find('div', 'place_half').text

                    subject = hb.find('div', 'subject_half').text

                    br = cell.find('div', 'break_top')

                    sp = hb.find('div', 'sp_half').text

                    if br != None:
                        return days[day] + '\nПара ' + subject + '\nв ' + times[i][
                            1] + '\n' + place + '\nпреподаватель ' + sp

                    return days[day] + '\nПара ' + subject + '\nв ' + times[i][
                        0] + '\n' + place + '\nпреподаватель ' + sp

                elif halfs[1].text != ' ' and week == 2:

                    hb = BeautifulSoup(str(halfs[1]), 'lxml')

                    place = hb.find('div', 'place_half').text

                    subject = hb.find('div', 'subject_half').text

                    br = cell.find('div', 'break_top')

                    sp = hb.find('div', 'sp_half').text

                    if br != None:
                        return days[day] + '\nПара ' + subject + '\nв ' + times[i][
                            1] + '\n' + place + '\nпреподаватель ' + sp

                    return days[day] + '\nПара ' + subject + '\nв ' + times[i][
                        0] + '\n' + place + '\nпреподаватель ' + sp
            else:

                place = cell.find('div', 'place_std').text

                subject = cell.find('div', 'subject_std').text

                br = cell.find('div', 'break_top')

                sp = cell.find('div', 'sp_std').text

                if br != None:
                    return days[day] + '\nПара ' + subject + '\nв ' + times[i][
                        1] + '\n' + place + '\nпреподаватель ' + sp

                return days[day] + '\nПара ' + subject + '\nв ' + times[i][0] + '\n' + place + '\nпреподаватель ' + sp

    else:
        return 'Пар нет'


def day_sсhedule(link, day):
    '''
    Выдает расписание на день
    :param link: ссылка на страницу с расписанием группы
    :param day: номер дня недели от 0(понедельник) до 6 (воскресенье), при не соблюдении условия функция вернет False
    :return: list, с парами и если есть часовиком
    '''
    if day > 6 or day < 0:
        return False

    r = requests.get(link)

    r.encoding = 'utf-8'

    doc = r.text

    soup = BeautifulSoup(doc, 'lxml')

    week = current_week()

    if day == 6:
        return 'Ввоскресенье же, отдыхай'

    table = soup.find_all('td', 'schedule_std')

    classes = []

    curr = []

    for i in range(6):
        curr.append(table[day + i * 6])

    for i in range(6):
        if curr[i].text != ' ':

            cell = BeautifulSoup(str(curr[i]), 'lxml')

            quarts = cell.find_all('td', 'schedule_quarter')

            halfs = cell.find_all('td', 'schedule_half')

            if quarts != [] and len(quarts) == 4:
                if quarts[0].text != ' ' and week == 1:

                    qb = BeautifulSoup(str(quarts[0]), 'lxml')

                    place = qb.find('div', 'place_quarter').text

                    subject = qb.find('div', 'subject_quarter').text

                    sp = qb.find('div', 'sp_quarter').text

                    if sp == ' ':
                        sp = ''
                    else:
                        sp = '\nпреподаватель ' + sp

                    classes.append(
                        'Полупара ' + subject + '\nв ' + times[i][0] + '\n' + place + sp)

                elif quarts[1].text != ' ' and week == 2:

                    qb = BeautifulSoup(str(quarts[0]), 'lxml')

                    place = qb.find('div', 'place_quarter').text

                    subject = qb.find('div', 'subject_quarter').text

                    sp = qb.find('div', 'sp_quarter').text

                    if sp == ' ':
                        sp = ''
                    else:
                        sp = '\nпреподаватель ' + sp

                    classes.append(
                        'Полупара ' + subject + '\nв ' + times[i][0] + '\n' + place + sp)

                if quarts[2].text != ' ' and week == 1:

                    qb = BeautifulSoup(str(quarts[0]), 'lxml')

                    place = qb.find('div', 'place_quarter').text

                    subject = qb.find('div', 'subject_quarter').text

                    sp = qb.find('div', 'sp_quarter').text

                    if sp == ' ':
                        sp = ''
                    else:
                        sp = '\nпреподаватель ' + sp

                    classes.append(
                        'Полупара ' + subject + '\nв ' + times[i][0] + '\n' + place + sp)

                elif quarts[3].text != ' ' and week == 2:

                    qb = BeautifulSoup(str(quarts[0]), 'lxml')

                    place = qb.find('div', 'place_quarter').text

                    subject = qb.find('div', 'subject_quarter').text

                    sp = qb.find('div', 'sp_quarter').text

                    if sp == ' ':
                        sp = ''
                    else:
                        sp = '\nпреподаватель ' + sp

                    classes.append(
                        'Полупара ' + subject + '\nв ' + times[i][0] + '\n' + place + sp)

            elif halfs != [] and len(halfs) == 2:

                if halfs[0].text != ' ' and week == 1:

                    hb = BeautifulSoup(str(halfs[0]), 'lxml')

                    place = hb.find('div', 'place_half').text

                    subject = hb.find('div', 'subject_half').text

                    br = hb.find('div', 'break_top')

                    br_bot = hb.find('div', 'break_bottom')

                    sp = hb.find('div', 'sp_half').text

                    if sp == ' ':
                        sp = ''
                    else:
                        sp = '\nпреподаватель ' + sp

                    if br != None:
                        classes.append('Часовик в ' + times[i][0])
                        classes.append(
                            'Пара ' + subject + '\nв ' + times[i][1] + '\n' + place + sp)

                    classes.append(
                        'Пара ' + subject + '\nв ' + times[i][0] + '\n' + place + sp)

                    if br_bot != None:
                        classes.append('Часовик в ' + times[i][2])

                elif halfs[1].text != ' ' and week == 2:

                    hb = BeautifulSoup(str(halfs[1]), 'lxml')

                    place = hb.find('div', 'place_half').text

                    subject = hb.find('div', 'subject_half').text

                    br = hb.find('div', 'break_top')

                    br_bot = hb.find('div', 'break_bottom')

                    sp = hb.find('div', 'sp_half').text

                    if sp == ' ':
                        sp = ''
                    else:
                        sp = '\nпреподаватель ' + sp

                    if br != None:
                        classes.append('Часовик в ' + times[i][0])
                        classes.append(
                            'Пара ' + subject + '\nв ' + times[i][1] + '\n' + place + sp)

                    classes.append(
                        'Пара ' + subject + '\nв ' + times[i][0] + '\n' + place + sp)

                    if br_bot != None:
                        classes.append('Часовик в ' + times[i][2])

            else:

                place = cell.find('div', 'place_std').text

                subject = cell.find('div', 'subject_std').text

                br = cell.find('div', 'break_top')

                br_bot = cell.find('div', 'break_bottom')

                sp = cell.find('div', 'sp_std').text

                if sp == ' ':
                    sp = ''
                else:
                    sp = '\nпреподаватель ' + sp

                if br != None:
                    classes.append('Часовик в ' + times[i][0])
                    classes.append(
                        'Пара ' + subject + '\nв ' + times[i][1] + '\n' + place + sp)

                classes.append(
                    'Пара ' + subject + '\nв ' + times[i][0] + '\n' + place + sp)

                if br_bot != None:
                    classes.append('Часовик в ' + times[i][2])

    if classes == []:
        return 'Пар нет'

    return classes


# t = time.gmtime(time.time())
# day = t[6]

propose_records = telepot.helper.SafeDict()  # thread-safe dict


class Sheduler(telepot.helper.ChatHandler):
    markup = ReplyKeyboardMarkup(keyboard=[
        [KeyboardButton(text='Шо сегодня'), KeyboardButton(text='Шо там завтра')],

        [KeyboardButton(text='Выбрать день'), KeyboardButton(text='Когда в шарагу')],

        [KeyboardButton(text='Сменить группу'), KeyboardButton(text='Какая это неделя?')],

    ], resize_keyboard=True)

    rem_mark = ReplyKeyboardRemove()

    days_inline = InlineKeyboardMarkup(inline_keyboard=[
                    [InlineKeyboardButton(text=days[0], callback_data='mon')],
                    [InlineKeyboardButton(text=days[1], callback_data='tue')],
                    [InlineKeyboardButton(text=days[2], callback_data='wed')],
                    [InlineKeyboardButton(text=days[3], callback_data='thu')],
                    [InlineKeyboardButton(text=days[4], callback_data='fri')],
                    [InlineKeyboardButton(text=days[5], callback_data='sat')],
    ])

    def __init__(self, *args, **kwargs):
        super(Sheduler, self).__init__(*args, **kwargs)

        # Retrieve from database
        global propose_records
        if self.id in propose_records:
            self._group, self._link, self._edit_msg_ident = propose_records[self.id]
            self._editor = telepot.helper.Editor(self.bot, self._edit_msg_ident) if self._edit_msg_ident else None
        else:
            self._group = ''
            self._link = ''
            self._edit_msg_ident = None
            self._editor = None


    def _cancel_last(self):
        if self._editor:
            self._editor.editMessageReplyMarkup(reply_markup=None)
            self._editor = None
            self._edit_msg_ident = None

    def on_chat_message(self, msg):

        if msg.get('text') == None:
            print(msg['sticker']['file_id'])
            print(msg['sticker']['emoji'])

        elif msg['text'] == 'Выбрать день' and self._group != '':

            sent = self.sender.sendMessage('Расписание на', reply_markup=self.days_inline)
            self._editor = telepot.helper.Editor(self.bot, sent)
            self._edit_msg_ident = telepot.message_identifier(sent)

        elif msg['text'] == 'Сменить группу' and self._group != '':

            sent = self.sender.sendMessage('Введите название вашей группу, например ит-41', reply_markup=self.rem_mark)
            self._editor = telepot.helper.Editor(self.bot, sent)
            self._edit_msg_ident = telepot.message_identifier(sent)
            self._group = ''


        elif msg['text'] == 'Шо сегодня' and self._group != '':

            t = time.localtime(time.time())
            day = t[6]
            responce = day_sсhedule(self._link, day)

            res = ''


            if day == 6:
                res = 'Воскресенье же отдыхай'

            elif responce != 'Пар нет':
                for el in responce:
                    res = res + el + '\n\n'
            else:
                res = responce

            sent = self.sender.sendMessage(res, reply_markup=self.markup)
            self._editor = telepot.helper.Editor(self.bot, sent)
            self._edit_msg_ident = telepot.message_identifier(sent)

        elif msg['text'] == 'Когда в шарагу' and self._group != '':

            t = time.localtime(time.time())
            day = t[6]

            if t[3] >= 19:
                if day == 5:
                    day = 0
                else:
                    day += 1

            responce = first_para(self._link, day)

            while responce == 'Пар нет':
                day += 1
                responce = first_para(self._link, day)

            self.sender.sendSticker('CAADAgADMAADNraOCPwLBo-827_NAg')

            sent = self.sender.sendMessage(responce, reply_markup=self.markup)
            self._editor = telepot.helper.Editor(self.bot, sent)
            self._edit_msg_ident = telepot.message_identifier(sent)

        elif msg['text'] == 'Какая это неделя?' and self._group != '':

            week = current_week()

            if week == 1:
                sent = self.sender.sendMessage('Числитель', reply_markup=self.markup)
                self._editor = telepot.helper.Editor(self.bot, sent)
                self._edit_msg_ident = telepot.message_identifier(sent)
            else:
                sent = self.sender.sendMessage('Знаменатель', reply_markup=self.markup)
                self._editor = telepot.helper.Editor(self.bot, sent)
                self._edit_msg_ident = telepot.message_identifier(sent)

        elif msg['text'] == 'Шо там завтра' and self._group != '':

            t = time.localtime(time.time())
            day = t[6]

            if day == 6:
                day = 0
            else:
                day += 1


            responce = day_sсhedule(self._link, day)

            res = ''

            if day == 6:
                res = 'Алло завтра воскресенье'

            elif responce != 'Пар нет':
                for el in responce:
                    res = res + el + '\n\n'
            else:
                res = responce

            sent = self.sender.sendMessage(res, reply_markup=self.markup)
            self._editor = telepot.helper.Editor(self.bot, sent)
            self._edit_msg_ident = telepot.message_identifier(sent)


        elif msg['text'] == '/start':
            sent = self.sender.sendMessage('Введите название вашей группу, например ит-41', reply_markup=self.rem_mark)
            self._editor = telepot.helper.Editor(self.bot, sent)
            self._edit_msg_ident = telepot.message_identifier(sent)
        elif self._group == '':
            group = msg['text']
            res = group_search(group)
            if res != False:
                self._group = group
                self._link = res
                sent = self.sender.sendMessage('Группа установлена', reply_markup=self.markup)
                self._editor = telepot.helper.Editor(self.bot, sent)
                self._edit_msg_ident = telepot.message_identifier(sent)
            else:
                sent = self.sender.sendMessage('Такой группы нет, повторите ввод', reply_markup=self.rem_mark)
                self._editor = telepot.helper.Editor(self.bot, sent)
                self._edit_msg_ident = telepot.message_identifier(sent)



                # self._propose()

    def on_callback_query(self, msg):
        query_id, from_id, query_data = telepot.glance(msg, flavor='callback_query')

        if query_data == 'mon':
            self._cancel_last()

            responce = day_sсhedule(self._link, 0)

            res = ''

            if responce != 'Пар нет':
                for el in responce:
                    res = res + el + '\n\n'
            else:
                res = responce

            self.sender.sendMessage(days[0], reply_markup=None)

            sent = self.sender.sendMessage(res, reply_markup=self.markup)
            self._editor = telepot.helper.Editor(self.bot, sent)
            self._edit_msg_ident = telepot.message_identifier(sent)

        elif query_data == 'tue':
            self._cancel_last()

            responce = day_sсhedule(self._link, 1)

            res = ''

            if responce != 'Пар нет':
                for el in responce:
                    res = res + el + '\n\n'
            else:
                res = responce

            self.sender.sendMessage(days[1], reply_markup=None)

            sent = self.sender.sendMessage(res, reply_markup=self.markup)
            self._editor = telepot.helper.Editor(self.bot, sent)
            self._edit_msg_ident = telepot.message_identifier(sent)

        elif query_data == 'wed':
            self._cancel_last()

            responce = day_sсhedule(self._link, 2)

            res = ''

            if responce != 'Пар нет':
                for el in responce:
                    res = res + el + '\n\n'
            else:
                res = responce

            self.sender.sendMessage(days[2], reply_markup=None)

            sent = self.sender.sendMessage(res, reply_markup=self.markup)
            self._editor = telepot.helper.Editor(self.bot, sent)
            self._edit_msg_ident = telepot.message_identifier(sent)

        elif query_data == 'thu':
            self._cancel_last()

            responce = day_sсhedule(self._link, 3)

            res = ''

            if responce != 'Пар нет':
                for el in responce:
                    res = res + el + '\n\n'
            else:
                res = responce

            self.sender.sendMessage(days[3], reply_markup=None)

            sent = self.sender.sendMessage(res, reply_markup=self.markup)
            self._editor = telepot.helper.Editor(self.bot, sent)
            self._edit_msg_ident = telepot.message_identifier(sent)

        elif query_data == 'fri':
            self._cancel_last()

            responce = day_sсhedule(self._link, 4)

            res = ''

            if responce != 'Пар нет':
                for el in responce:
                    res = res + el + '\n\n'
            else:
                res = responce

            self.sender.sendMessage(days[4], reply_markup=None)

            sent = self.sender.sendMessage(res, reply_markup=self.markup)
            self._editor = telepot.helper.Editor(self.bot, sent)
            self._edit_msg_ident = telepot.message_identifier(sent)

        elif query_data == 'sat':
            self._cancel_last()

            responce = day_sсhedule(self._link, 5)

            res = ''

            if responce != 'Пар нет':
                for el in responce:
                    res = res + el + '\n\n'
            else:
                res = responce

            self.sender.sendMessage(days[5], reply_markup=None)

            sent = self.sender.sendMessage(res, reply_markup=self.markup)
            self._editor = telepot.helper.Editor(self.bot, sent)
            self._edit_msg_ident = telepot.message_identifier(sent)


    def on__idle(self, event):

        t = time.localtime(time.time())

        if (t[3] == 2 and t[4] == 13 and t[5] == 0 and self._group != ''):
            sent = self.sender.sendMessage('Просыпайся пидор пары скоро', reply_markup=self.markup)
            self._editor = telepot.helper.Editor(self.bot, sent)
            self._edit_msg_ident = telepot.message_identifier(sent)

        self.close()

    def on_close(self, ex):
        # Save to database
        global propose_records
        propose_records[self.id] = (self._group, self._link, self._edit_msg_ident)


TOKEN = '342080385:AAHQRtFEyegac8pFw7WTBQBltmNc79JJvB0'

bot = telepot.DelegatorBot(TOKEN, [
    include_callback_query_chat_id(
        pave_event_space())(
        per_chat_id(types=['private']), create_open, Sheduler, timeout=10),
])

up = bot.getUpdates()




MessageLoop(bot).run_as_thread()
print('Listening ...')

while 1:
    time.sleep(10)

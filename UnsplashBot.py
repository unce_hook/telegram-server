from unsplash_python.unsplash import Unsplash
import requests
import json
import urllib.request
import telepot
from telepot.loop import MessageLoop
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton
import time

unsplash = Unsplash({
    'application_id': '97e80729a798360d8a08789bdd26081f01e15cdd72eab83a0851e822b5036d13',
    'secret': '042032b44491007e14d40fde3cccb89fb1504cad8971df545e9dafe465ee6932',
    'callback_url': ''})

def get_photo():
    '''
    return info about photo
    :return: url - source photo url
    user - name of photographer
    description - description about photo
    categories - cats in photo
    '''
    im = unsplash.photos().get_random_photo(
        featured=True
    )

    url = [0,0]
    url[0] = im[0]['urls']['raw']
    url[1] = im[0]['urls']['full']
    description = im[0]['description']
    categories = im[0]['categories']
    user = im[0]['user']['name']

    return url, user, description, categories

def handle(msg):
    content_type, chat_type, chat_id = telepot.glance(msg)
    print(content_type, chat_type, chat_id)

TOKEN = '466986402:AAFJ438joYAasV7sCW_I9HwwiezZL05b41Y'  # get token from command-line

bot = telepot.Bot(TOKEN)
MessageLoop(bot, handle).run_as_thread()
print('Listening ...')

state = 1
while True:
    t = time.localtime(time.time())
    if state == 1 and t[3] % 3 == 0 and t[4] == 0:
        url, user, description, categories = get_photo()
        try:
            description = description[:140]
            response = description + '\nPhoto by ' + user + '.'
            for cat in categories:
                response += '#' + cat['title']
            link_markup = InlineKeyboardMarkup(inline_keyboard=[
                [InlineKeyboardButton(text='Download', url=url[0])],
            ])
            bot.sendPhoto('-1001336176081', url[1], caption=response, reply_markup=link_markup)

        except Exception:
            state = 1
        else:
            state = 2

    elif state == 2 and t[4] != 0:
        state = 1
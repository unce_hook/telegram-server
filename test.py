import background

# Use 40 background threads.
background.n = 4


@background.task
def work():
    import time
    time.sleep(10)
    print(time.time())

@background.callback
def work_callback(future):
    print(future)


for _ in range(100):
    work()